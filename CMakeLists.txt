cmake_minimum_required (VERSION 2.8)

# Projectname
project(Test)

# Include opencv
find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )

# Source
add_executable(Test test_code.cpp)

# Libraries
target_link_libraries( Test ${OpenCV_LIBS} )
